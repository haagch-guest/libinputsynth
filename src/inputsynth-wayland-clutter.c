/*
 * LibInputSynth
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "inputsynth-wayland-clutter.h"

struct _InputSynthWaylandClutter
{
  InputSynth parent;

  ClutterVirtualInputDevice *virt_dev;
};

G_DEFINE_TYPE (InputSynthWaylandClutter, input_synth_wayland_clutter, INPUT_TYPE_SYNTH)

InputSynthWaylandClutter *
input_synth_wayland_clutter_new (void)
{
  InputSynthWaylandClutter *self = (InputSynthWaylandClutter*)
      g_object_new (INPUT_TYPE_SYNTH_WAYLAND_CLUTTER, 0);
  return self;
}

static void
input_synth_wayland_clutter_init (InputSynthWaylandClutter *self)
{
  ClutterDeviceManager *manager = clutter_device_manager_get_default ();

  // TODO: check if the virtual pointer is supported
  // ClutterVirtualDeviceType supported_types =
  //   clutter_device_manager_get_supported_virtual_device_types (manager);

  self->virt_dev =
    clutter_device_manager_create_virtual_device (manager,
                                                  CLUTTER_POINTER_DEVICE);

}

static void
click_wayland_clutter (InputSynth *self_parent, int x, int y,
                       int button, gboolean press)
{
  (void) x;
  (void) y;

  InputSynthWaylandClutter *self = INPUT_SYNTH_WAYLAND_CLUTTER (self_parent);

  uint32_t clutter_button = (button == 2 ? 3 : (uint32_t) button);
  clutter_virtual_input_device_notify_button (self->virt_dev,
                                              CLUTTER_CURRENT_TIME,
                                              clutter_button,
                                              press ?
                                                CLUTTER_BUTTON_STATE_PRESSED :
                                                CLUTTER_BUTTON_STATE_RELEASED);
}

static void
move_cursor_wayland_clutter (InputSynth *self_parent, int x, int y)
{
  InputSynthWaylandClutter *self = INPUT_SYNTH_WAYLAND_CLUTTER (self_parent);

  clutter_virtual_input_device_notify_absolute_motion (self->virt_dev,
                                                       CLUTTER_CURRENT_TIME,
                                                       x, y);
}

static void
character_wayland_clutter (InputSynth *self_parent, char character)
{
  InputSynthWaylandClutter *self = INPUT_SYNTH_WAYLAND_CLUTTER (self_parent);
  /* TODO */
  (void) character;
  (void) self;
  g_printerr ("character input unsupported in this backend\n");
}

static void
input_synth_wayland_clutter_finalize (GObject *gobject)
{
  InputSynthWaylandClutter *self = INPUT_SYNTH_WAYLAND_CLUTTER (gobject);

  /* TODO: check if this is enough */
  g_object_unref (self->virt_dev);

  self->virt_dev = NULL;

  G_OBJECT_CLASS (input_synth_wayland_clutter_parent_class)->finalize (gobject);
}

static GString *
get_backend_name_wayland_clutter (InputSynth *self_parent)
{
  (void) self_parent;
  return g_string_new ("Clutter on Wayland");
}

static void
input_synth_wayland_clutter_class_init (InputSynthWaylandClutterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = input_synth_wayland_clutter_finalize;

  InputSynthClass *input_synth_class = INPUT_SYNTH_CLASS (klass);
  input_synth_class->click = click_wayland_clutter;
  input_synth_class->move_cursor = move_cursor_wayland_clutter;
  input_synth_class->character = character_wayland_clutter;
  input_synth_class->get_backend_name = get_backend_name_wayland_clutter;
}

